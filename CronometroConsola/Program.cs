﻿namespace CronometroConsola
{
    using System;
    using ExercicioCronometro;

    class Program
    {
        static void Main(string[] args)
        {
            var relogio = new Cronometro();
            Console.WriteLine("Pressione Enter para iniciar o cronómetro");
            Console.ReadLine();

            relogio.StartClock();
            Console.WriteLine("Pressione Enter para parar o cronómetro");

            while (relogio.ClockStats())
            {
                var tempo = DateTime.Now - relogio.StarTime();
                Console.Write("\r Tempo Corrente: {0}", tempo);

                if (Console.KeyAvailable) // se a tecla estiver disponivel
                {
                    if (Console.ReadKey().Key == ConsoleKey.Enter) // se a tecla foi enter
                    {
                        break;
                    }
                }
            }

            relogio.StopClock();

            Console.WriteLine("\r Tempo Cronometrado: {0}", relogio.GetTimeSpan());
            Console.ReadLine();
        }
    }
}
