﻿namespace ExercicioCronometro
{
    using System;

    public class Cronometro
    {

        #region Atributos

        private DateTime _start;
        private DateTime _stop;
        private bool _isrunning;

        #endregion

        public void StartClock()
        {
            if (_isrunning)
                throw new InvalidOperationException("O cronómetro já está ligado!");
            // Lança uma execeção
            // Interrompe o porgrama e manda uma mensagem

            _start = DateTime.Now;
            _isrunning = true;
        }

        public void StopClock()
        {
            if (!_isrunning)
                throw new InvalidOperationException("O cronómetro já está desligado!");
            // Lança uma execeção
            // Interrompe o porgrama e manda uma mensagem
          
            _stop = DateTime.Now;
            _isrunning = false;

        }

        //TimeSpan para fazer contas com o tempo
        public TimeSpan GetTimeSpan()
        {
            return _stop - _start;
        }

        public bool ClockStats()
        {
            return _isrunning;
        }

        public DateTime StarTime()
        {
            return _start;
        }
    }
}
