﻿namespace ExercicioCronometro
{
    using System;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {

        private readonly Cronometro _cronometro; // Composição

        public Form1()
        {
            InitializeComponent();
            _cronometro = new Cronometro();
        }

        private void ButtonOnOff_Click(object sender, EventArgs e)
        {
            if (_cronometro.ClockStats())
            {
                // Desliga o cronometro
                _cronometro.StopClock();
                ButtonOnOff.Text = "Liga";
                TimerRelogio.Enabled = false;
                //LabelContador.Text = _cronometro.GetTimeSpan().ToString();
            }
            else
            {
                // Liga o cronometro
                _cronometro.StartClock();
                ButtonOnOff.Text = "Desliga";
                TimerRelogio.Enabled = true;
            }
        }

        private void UpdateLabel()
        {
            var tempo = DateTime.Now - _cronometro.StarTime(); // var fica o tipo que captar

            LabelContador.Text = string.Format("{0:D2}:{1:D2}:{2:D2}:{3:D3}", tempo.Hours, tempo.Minutes, tempo.Seconds, tempo.Milliseconds);
        }

        private void TimerRelogio_Tick(object sender, EventArgs e)
        {
            UpdateLabel();
        }
    }
}
